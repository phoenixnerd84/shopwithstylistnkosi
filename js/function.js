
function cart(){
    if(localStorage.getItem('products')){
        $(window).on('load', function() {
            var products = JSON.parse(localStorage.getItem('products'));
            $("#cart").text(products.cart);
        });
    } else{
        var products = {
            'cart' : 0,
            'items' : []
        }
        localStorage.setItem('products', JSON.stringify(products));
        $(window).on('load', function() {
            var products = JSON.parse(localStorage.getItem('products'));
            $("#cart").text(products.cart);
        });
    }

}


function add_to_cart(id) {
    $.post('https://andriesbingani.co.za/Stylist/destination.php', {
            type: "getItemById",
            id: id
        },
        function(data) {
            var product = JSON.parse(data);
            var products = JSON.parse(localStorage.getItem('products'));
            var added = false;
            for(var i =0; i < products.items.length; i++){
                if(product.info[0].name === products.items[i].name){
                    added = true;
                    products.items[i].quantity += 1;
                    break;
                }
            }
            if(!added){
                product.info[0].quantity = 1;
                product.info[0].size = 3;
                products.items.push(product.info[0]);
                products.cart += 1;
            }

            $("#cart").text(products.cart);
            localStorage.setItem('products', JSON.stringify(products));

            // Get the snackbar DIV
            var x = document.getElementById("snackbar");

            // Add the "show" class to DIV
            x.className = "show";

            // After 3 seconds, remove the show class from DIV
            setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
        }
    );
}

function get_details(id){
    localStorage.setItem('id', JSON.stringify(id));
    window.location.href ="single-product.html";
}

function view_details(){
    var id = localStorage.getItem('id');
    $.post('https://andriesbingani.co.za/Stylist/destination.php', {
            type: "getItemById",
            id: id
        },
        function(data) {
            var l = JSON.parse(data);
            $('#name').text(l.info[0].name);
            $('#price').text('R'+l.info[0].price);
            // $('#Stock').text(l.info[0].name);
            $('#description').text(l.info[0].description);
            $("#image_1").attr("src","img/product/"+l.info[0].image_name);
            // $("#image_1").attr("src","img/product/p7.jpg");
            //document.getElementById("image_1").src="img/product/"+l.info[0].image_name1;
            $('#addToCart').append(
                "<a class=\"primary-btn\" onclick=\"add_to_cart("+id+")\">Add to Cart</a>"
            );
            $('.owl-controls').html('')
	    }
    );


}

function getLatestProducts(){
    $.post('https://andriesbingani.co.za/Stylist/destination.php', {
            type: "getItems"
        },
        function(data) {
            //console.log(name);
            var l = JSON.parse(data);
            var product = l.info.filter(function (el) { 
                return el.category !== 'Special';
            });
            var max = product.length < 5 ? product.length-1 : 4;
            var min = max-4 > -1 ? max-4 : -1;
            for(var i = max; i > min; i--){
                $("#latest").append(
                "<div class=\"col-lg-3 col-md-6\" >"+
                    "<div class=\"single-product\">"+
                        "<a style=\"cursor:pointer\" onclick=\"get_details("+product[i].id+")\"><img class=\"img-fluid\" src=\"img/product/"+product[i].image_name+"\" ></a>"+
                        "<div class=\"product-details justify-items-center\">"+
                            "<a style=\"cursor:pointer\" onclick=\"get_details("+product[i].id+")\">"+
                            "<h6>"+product[i].name+"</h6>"+
                            "<div class=\"price\">"+
                                "<h6>R"+product[i].price+"</h6>"+
                                "<h6 class=\"l-through\">R"+product[i].price_2+"</h6>"+
                            "</div>"+
                            "</a>"+
                            "<div class=\"prd-bottom d-flex\">"+
                                "<a onclick=\"add_to_cart("+product[i].id+")\" class=\"social-info\">"+
                                    "<span class=\"ti-bag\"></span>"+
                                    "<p class=\"hover-text\">add to bag</p>"+
                                "</a>"+
                                "<a onclick=\"get_details("+product[i].id+")\" class=\"social-info\">"+
                                    "<span class=\"lnr lnr-move\"></span>"+
                                    "<p class=\"hover-text\">view more</p>"+
                                "</a>"+
                            "</div>"+
                        "</div>"+
                    "</div>"+
                "</div>"
            );
            }
            var special = l.info.filter(function (el) { 
                return el.category === 'Special';
            });
            max = special.length < 5 ? special.length-1 : 4;
            min = max-4 > -1 ? max-4 : -1;
            for(var i = max; i > min; i--){
                $("#latestSpecials").append(
                "<div class=\"col-lg-3 col-md-6\" >"+
                    "<div class=\"single-product\">"+
                        "<a style=\"cursor:pointer\" onclick=\"get_details("+special[i].id+")\"><img class=\"img-fluid\" src=\"img/product/"+special[i].image_name+"\" ></a>"+
                        "<div class=\"product-details justify-items-center\">"+
                            "<a style=\"cursor:pointer\" onclick=\"get_details("+special[i].id+")\">"+
                            "<h6>"+special[i].name+"</h6>"+
                            "<div class=\"price\">"+
                                "<h6>R"+special[i].price+"</h6>"+
                                "<h6 class=\"l-through\">R"+special[i].price_2+"</h6>"+
                            "</div>"+
                            "</a>"+
                            "<div class=\"prd-bottom d-flex\">"+
                                "<a onclick=\"add_to_cart("+special[i].id+")\" class=\"social-info\">"+
                                    "<span class=\"ti-bag\"></span>"+
                                    "<p class=\"hover-text\">add to bag</p>"+
                                "</a>"+
                                "<a onclick=\"get_details("+special[i].id+")\" class=\"social-info\">"+
                                    "<span class=\"lnr lnr-move\"></span>"+
                                    "<p class=\"hover-text\">view more</p>"+
                                "</a>"+
                            "</div>"+
                        "</div>"+
                    "</div>"+
                "</div>"
            );
            }
	    }
    );
    
}

function getProducts(special, page, brand, category){
    $("#products").html("");
    $("#products").html("<p>loading products...</p>");
    $.post('https://andriesbingani.co.za/Stylist/destination.php', {
            type: "getItems"
        },
        function(data) {
            //console.log(name);
            var l = JSON.parse(data);
            l.info = l.info.filter(function (el) { 
                if(special === 'Special') return el.category === special;
                else return el.category !== 'Special';
            });
            var show = $("#show").val() === 'all' ? l.info.length : +$("#show").val();
            var newArray = l.info.filter(function (el) { return el.category === 'Shoes' });
            var brands = newArray.map(item => item.brand).filter((value, index, self) => self.indexOf(value) === index);
            $("#shoes").html("");
            for(var i = 0; i < brands.length; i++){
                $("#shoes").append(
                    "<li class=\"main-nav-list child\"><a onclick=\" getProducts('"+special+"',"+1+",'"+brands[i]+"','Shoes')\">"+brands[i]+"<span class=\"number\"></span></a></li>"
                );
            }
            newArray = l.info.filter(function (el) { return el.category === 'Clothes' });
            brands = newArray.map(item => item.brand).filter((value, index, self) => self.indexOf(value) === index);
            $("#clothes").html("");
            for(var i = 0; i < brands.length; i++){
                $("#clothes").append(
                    "<li class=\"main-nav-list child\"><a onclick=\" getProducts('"+special+"',"+1+",'"+brands[i]+"','Clothes')\">"+brands[i]+"<span class=\"number\"></span></a></li>"
                );
            }
            newArray = l.info.filter(function (el) { return el.category === 'Accessories' });
            brands = newArray.map(item => item.brand).filter((value, index, self) => self.indexOf(value) === index);
            $("#accessories").html("");
            for(var i = 0; i < brands.length; i++){
                $("#accessories").append(
                    "<li class=\"main-nav-list child\"><a onclick=\" getProducts('"+special+"',"+1+",'"+brands[i]+"','Accessories')\">"+brands[i]+"<span class=\"number\"></span></a></li>"
                );
            }
            var filteredList = l.info.filter(function (el) { return el.category === category && el.brand === brand});
            if(filteredList.length) l.info = filteredList; 
            
            var max = l.info.length <= show ? l.info.length : show;
            if($("#sort").val() === 'nameA' ){
                l.info = l.info.sort(function (a, b) {
                    return a.name > b.name ;
                });
            } else if($("#sort").val() === 'nameD' ){
                l.info = l.info.sort(function (a, b) {
                    return a.name < b.name ;
                });
            } else if($("#sort").val() === 'priceL' ){
                l.info = l.info.sort(function (a, b) {
                    return +a.price > +b.price;
                });
            } else if($("#sort").val() === 'priceH'){
                l.info = l.info.sort(function (a, b) {
                    return +a.price < +b.price;
                });
            }
            $("#products").html("");
            var numGroups = Math.ceil(l.info.length/max);
            page = page ? page : 1;
            brand = brand ? brand : '';
            category = category ? category : '';
            for(var i = page*max-max; i < page*max && i < l.info.length; i++){
                if((brand === '' || brand === l.info[i].brand) && (brand === '' || category === l.info[i].category) ){
                    $("#products").append(
                        "<div class=\"col-lg-3 col-md-6\" >"+
                            "<div class=\"single-product\">"+
                                "<a style=\"cursor:pointer\" onclick=\"get_details("+l.info[i].id+")\"><img class=\"img-fluid\" src=\"img/product/"+l.info[i].image_name+"\" >"+
                                "<div class=\"product-details\">"+
                                    "<h6>"+l.info[i].name+"</h6>"+
                                    "<div class=\"price\">"+
                                        "<h6>R"+l.info[i].price+"</h6>"+
                                        "<h6 class=\"l-through\">R"+l.info[i].price_2+"</h6>"+
                                    "</div>"+
                                    "</a>"+
                                    "<div class=\"prd-bottom d-flex\">"+
                                        "<a onclick=\"add_to_cart("+l.info[i].id+")\" class=\"social-info\">"+
                                            "<span class=\"ti-bag\"></span>"+
                                            "<p class=\"hover-text\">add to bag</p>"+
                                        "</a>"+
                                        "<a onclick=\"get_details("+l.info[i].id+")\" class=\"social-info\">"+
                                            "<span class=\"lnr lnr-move\"></span>"+
                                            "<p class=\"hover-text\">view more</p>"+
                                        "</a>"+
                                    "</div>"+
                                "</div>"+
                            "</div>"+
                        "</div>"
                    );
                }
            }
            $('.pagination').html('');
            
    
            if(numGroups != 1){
                var prevPage = +page-1;
                var nextPage = +page+1;
                if(page > 1){
                    $('.pagination').append(
                        "<a onclick=\" getProducts('"+special+"',"+prevPage+")\" class=\"prev-arrow\"><i class=\"fa fa-long-arrow-left\" aria-hidden=\"true\"></i></a>"+
                        "<a onclick=\"getProducts('"+special+"',1)\">"+1+"</a>"
                    );
                }
                if(page > 2){
                    $('.pagination').append(
                        "<a class=\"dot-dot\"><i class=\"fa fa-ellipsis-h\" aria-hidden=\"true\"></i></a>"
                    )
                } 
                $('.pagination').append(
                    "<a class=\"active\">"+page+"</a>"
                )

                if(page < numGroups-1){
                    $('.pagination').append(
                        "<a class=\"dot-dot\"><i class=\"fa fa-ellipsis-h\" aria-hidden=\"true\"></i></a>"
                    )
                }
                if(page < numGroups){
                    $('.pagination').append(
                        "<a onclick=\"getProducts('"+special+"',"+numGroups+")\">"+numGroups+"</a>"+
                        "<a onclick=\"getProducts('"+special+"',"+nextPage+")\" class=\"next-arrow\"><i class=\"fa fa-long-arrow-right\" aria-hidden=\"true\"></i></a>"
                    )
                }
                
            } else{
                $('#bottom').html('');
            }
            $('#shoesNumber').text('('+$("#shoes").children().length+')');
            $('#clothesNumber').text('('+$("#clothes").children().length+')');
            $('#accessoriesNumber').text('('+$("#accessories").children().length+')');
	    }
    );
    
}

function generate_table() {
    var tr;
    $('#emp_body').html('');
    for (var i = 0; i < displayRecords.length; i++) {
          tr = $('<tr/>');
          tr.append("<td>" + displayRecords[i].employee_name + "</td>");
          tr.append("<td>" + displayRecords[i].employee_salary + "</td>");
          tr.append("<td>" + displayRecords[i].employee_age + "</td>");
          $('#emp_body').append(tr);
    }
}

function apply_pagination() {
    $pagination.twbsPagination({
          totalPages: totalPages,
          visiblePages: 6,
          onPageClick: function (event, page) {
                displayRecordsIndex = Math.max(page - 1, 0) * recPerPage;
                endRec = (displayRecordsIndex) + recPerPage;
               
                displayRecords = records.slice(displayRecordsIndex, endRec);
                generate_table();
          }
    });
}

function productSummary(){
    var products = JSON.parse(localStorage.getItem('products'));
    var total = 0;
    for(var i = 0; i < products.items.length; i++){
        $("#productTotal").append(
            "<li><a>"+products.items[i].name +"<span class=\"middle\"> x"+products.items[i].quantity+"</span> <span class=\"last\">R"+products.items[i].price+"</span></a></li>"
        );
        total += +products.items[i].price*+products.items[i].quantity;
    }
    if(products.items.length > 1){
        $("#productTotal").append(
            "<li><a> FREE Cuban Links<span class=\"middle\"> x 1</span> <span class=\"last\">R0</span></a></li>"
        );
    }
    $("#subtotal").text(`R${total}`);
    $("#total").text(`R${total}`);
}


function checkout() {
    // $(window).on('load', function() {
    var products = JSON.parse(localStorage.getItem('products'));
    var total = 0;
        for(var i = 0; i < products.items.length; i++){
            total += products.items[i].price*products.items[i].quantity;
            $("#tableRow").append(
              "<tr>"+
                "<td>"+
                    "<div class=\"media\">"+
                        "<div class=\"d-flex\">"+
                            "<img class=\"img-fluid\" src=\"img/product/"+products.items[i].image_name+"\" alt=\"\">"+
                        "</div>"+
                        "<div class=\"media-body\">"+
                            "<p>"+products.items[i].name+"</p>"+
                        "</div>"+
                    "</div>"+
                "</td>"+
                "<td>"+
                    "<h5>R"+products.items[i].price+"</h5>"+
                "</td>"+
                "<td>"+
                    "<div class=\"product_count\">"+
                        "<input type=\"text\" name=\"size\" id=\"size\" maxlength=\"12\" value=\""+products.items[i].size+"\" title=\"Size:\""+
                            "class=\"input-text qty\">"+
                        "<button onclick=\"add_size("+products.items[i].id+")\""+
                            "class=\"increase items-count\" type=\"button\"><i class=\"lnr lnr-chevron-up\"></i></button>"+
                        "<button onclick=\"remove_size("+products.items[i].id+")\""+
                            "class=\"reduced items-count\" type=\"button\"><i class=\"lnr lnr-chevron-down\"></i></button>"+
                    "</div>"+
                "</td>"+
                "<td>"+
                    "<div class=\"product_count\">"+
                        "<input type=\"text\" name=\"qty\" id=\"sst\" maxlength=\"12\" value=\""+products.items[i].quantity+"\" title=\"Quantity:\""+
                            "class=\"input-text qty\">"+
                        "<button onclick=\"add_quantity("+products.items[i].id+")\""+
                            "class=\"increase items-count\" type=\"button\"><i class=\"lnr lnr-chevron-up\"></i></button>"+
                        "<button onclick=\"remove_quantity("+products.items[i].id+")\""+
                            "class=\"reduced items-count\" type=\"button\"><i class=\"lnr lnr-chevron-down\"></i></button>"+
                    "</div>"+
                "</td>"+
                "<td>"+
                    "<h5>R"+products.items[i].price*products.items[i].quantity+"</h5>"+
                "</td>"+
            "</tr>"
            );
        }
        $("#tableRow").append(
            "<tr>"+
                "<td></td>"+
                "<td></td>"+
                "<td></td>"+
                "<td>"+
                    "<h5>Subtotal</h5>"+
                "</td>"+
                "<td>"+
                    "<h5>R"+total+"</h5>"+
                "</td>"+
            "</tr>"
        );
        $('#tableRow').append(
            "<tr class=\"out_button_area\">"+
                "<td></td>"+
                "<td></td>"+
                "<td></td>"+
                "<td></td>"+
                "<td>"+
                    "<div class=\"checkout_btn_inner d-flex align-items-center\">"+
                        "<a class=\"gray_btn\" href=\"category.html\">Shop</a>"+
                        "<a class=\"primary-btn\" href=\"checkout.html\">Proceed</a>"+
                    "</div>"+
                "</td>"+
            "</tr>"
        )
    // });
}

function remove_quantity(item){
    var products = JSON.parse(localStorage.getItem('products'));
    for(var i =0; i < products.items.length; i++){
        if(item == products.items[i].id){
            products.items[i].quantity -= 1;
            if(products.items[i].quantity == 0){
                products.items.splice(i,1);
                products.cart -= 1;
                $("#cart").text(products.cart);
            }
            break;
        }
    }
    localStorage.setItem('products', JSON.stringify(products));

    $("#tableRow").html("");
    checkout();

}

function add_quantity(item){
    var products = JSON.parse(localStorage.getItem('products'));
    for(var i =0; i < products.items.length; i++){
        if(item == products.items[i].id){
            products.items[i].quantity += 1;
            break;
        }
    }
    localStorage.setItem('products', JSON.stringify(products));

    $("#tableRow").html("");
    checkout();
}

function remove_size(item){
    var products = JSON.parse(localStorage.getItem('products'));
    for(var i =0; i < products.items.length; i++){
        if(item == products.items[i].id){
            if(products.items[i].size == 3){
                break;
            }
            products.items[i].size -= 1;
            break;
        }
    }
    localStorage.setItem('products', JSON.stringify(products));

    $("#tableRow").html("");
    checkout();

}

function add_size(item){
    var products = JSON.parse(localStorage.getItem('products'));
    for(var i =0; i < products.items.length; i++){
        if(item == products.items[i].id){
            if(products.items[i].size == 9){
                break;
            }
            products.items[i].size += 1;
            break;
        }
    }
    localStorage.setItem('products', JSON.stringify(products));

    $("#tableRow").html("");
    checkout();
}



function pay_now(){
    var products = JSON.parse(localStorage.getItem('products'));
    var total = 0;
    for(var i = 0; i < products.items.length; i++){
        total += products.items[i].price*products.items[i].quantity;
    }
    var firstName = $('#first').val();
    var lastName = $('#last').val();
    var phone = $('#number').val();
    var address = $('#address').val();
    var specialInstructions = $('#specialInstructions').val();
    var type = "email";
    $('.error').remove();
    let valid = true;
    if(firstName.length < 3){
        $('#first').after('<span class="error" style="color:red"> Please enter your First Name</span>');
        valid = false;
    }
    if(lastName.length < 3){
        $('#last').after('<span class="error" style="color:red"> Please enter your Last Name</span>');
        valid = false;
    } 
    if(phone.length !== 10 && !isNaN(phone)){
        $('#number').after('<span class="error" style="color:red"> Please enter a valid phone</span>');
        valid = false;
    }
    if(address.length < 3){
        $('#address').after('<span class="error" style="color:red"> Please enter your address</span>');
        valid = false;
    } 
    if(!document.getElementById('f-option4').checked) {
        $('#tcs').after('<span class="error" style="color:red"> Please agree to the terms and conditions</span>');
        valid = false;
    }
    if(valid){
     // window.location.href = "https://www.payfast.co.za/eng/process?cmd=_paynow&amp;receiver=13089040&amp;item_name=diva+sauce+checkout&amp;item_description=Divasause&amp;amount=100.00&amp;return_url=%3A%2F%2Fandriesbingani.co.za&amp;cancel_url=%3A%2F%2Fandriesbingani.co.za";
        var details = {
            'firstName' : firstName,
            'lastName' : lastName,
            'phone' : phone,
            'address' : address,
            'specialInstructions' : specialInstructions
        };
        localStorage.setItem('details', JSON.stringify(details));
        // 13932685&amp;item_name=Shop+With+Stylist+Nkosi&amp;amount=10.00&amp;return_url=https%3A%2F%2Fandriesbingani.co.za&amp;cancel_url=https%3A%2F%2Fandriesbingani.co.za"><img src="https://www.payfast.co.za/images/buttons/light-small-paynow.png" width="165" height="36" alt="Pay" title="Pay Now with PayFast" /></a>
        $('#payButton').html("<a id=\"payfast\" href=\"https://www.payfast.co.za/eng/process?cmd=_paynow&amp;receiver=13932685&amp;item_name=Shop+With+Stylist+Nkosi&amp;amount="+total+"&amp;return_url=http%3A%2F%2Fshopwithstylistnkosi.co.za/confirmation.html&amp;cancel_url=http%3A%2F%2Fshopwithstylistnkosi.co.za/decline.html\"><img src=\"https://www.payfast.co.za/images/buttons/dark-small-paynow.png\" width=\"165\" height=\"36\" alt=\"Pay\" title=\"Pay Now with PayFast\" /></a>");
        $(document).ready(function(){
            document.getElementById('payfast').click();
        });
    } else{
        // Get the snackbar DIV
        var x = document.getElementById("snackbar");

        // Add the "show" class to DIV
        x.className = "show";

        // After 3 seconds, remove the show class from DIV
        setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
    }
    
}

function sendEmail() {
    $("#contactForm").submit(function(e) {
        e.preventDefault();
    });    
    var name = $('#name').val();
    var email = $('#email').val();
    var subject = $('#subject').val();
    var message = $('#message').val();
    var type = "email";
    var valid = true;

    $('.error').remove();
    if(name.length < 3){
        $('#name').after('<span class="error" style="color:red"> Please enter your Name</span>');
        valid = false;
    }
    if(!validateEmail(email)){
        $('#email').after('<span class="error" style="color:red"> Please enter a valid email</span>');
        valid = false;
    } 
    if(subject.length < 3){
        $('#subject').after('<span class="error" style="color:red"> Please enter your subject</span>');
        valid = false;
    } 
    if(message.length < 3){
        $('#message').after('<span class="error" style="color:red"> Please enter your type your message</span>');
        valid = false;
    } 
    if(valid){
        $.post('https://andriesbingani.co.za/destination.php', {
            name: name,
            email: email,
            subject: subject,
            message: message,
            receiver: "856807@students.wits.ac.za",
            type: type
        },
        function(data) {
            //console.log(name);
            var l = JSON.parse(data);
            if (l.success) $('#success').modal('show');
            else $('#error').modal('show');
	}
    );

    }
    
}

function validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  }

function successfulPayment() {
    var details = JSON.parse(localStorage.getItem('details'));
    var products = JSON.parse(localStorage.getItem('products'));
    //var message = `Hey there. Name: <b>${details.firstName} ${details.lastName}</b>, Phone Number: <b>${details.phone}</b> and Address: <b>{${details.address}}</b> just bought the following items. <b>${JSON.stringify(products.items)}</b>`;
    var message = `<!DOCTYPE html>
                    <html>
                    <body>		
                        <center>
                        <h2>Hi Admin!</h2>
                        <img src="https://andriesbingani.co.za/functions/logo.png" alt="Trulli" width="200" height="200">
                        <p>We have a new order!</p>
                        <p>Name<b>${details.name} ${details.lastName}</b></p>
                        <p>Phone Number: <b>${details.phone}</b></p>
                        <p>Address: <b>{${details.address}}</b></p> 
                        <p> just bought the following items. <b>${JSON.stringify(products.items)}</b></p>
                        </center>
                    </body>
                    </html>`;
    var type = "successfulPayment";
    localStorage.clear();
    $.post('https://andriesbingani.co.za/destination.php', {
            name: name,
            email: '',
            subject: 'subject',
            message: message,
            receiver: "856807@students.wits.ac.za",
            type: 'successfulPurchase'
        },
        function(data) {
            //console.log(name);
            var l = JSON.parse(data);
            if (l.success)
                $('#success').modal('show');
	}
    );

}

$("#show").change(function () {
    if (window.location.href.indexOf("category") > -1) {
        getProducts('Product');
      } else {
        getProducts('Special');
      }
    
});

$("#sort").change(function () {
    if (window.location.href.indexOf("category") > -1) {
        getProducts('Product');
      } else {
        getProducts('Special');
      }
});